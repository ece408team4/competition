/******************************************************************************
 *cr
 *cr         (C) Copyright 2010-2013 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ******************************************************************************/

// ============================================================================
// NOTE TO INSTRUCTORS: SOLUTION CODES ARE FOR INSTRUCTOR REFERENCE ONLY.
//    PLEASE DO NOT SHARE WITH STUDENTS
// ============================================================================

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "support.h"

void verify(float *A, float *B, float *C, int n) {

  const float relativeTolerance = 1e-4;//6;
  float s;
  float *X = (float*) malloc(n * sizeof(float));
  for(int i = 0; i < n; i++) {
    s = 0;
    for(int j = 0; j < i; j++){
	s += A[i * n + j] * X[j];
    }
    X[i]=( B[i] - s)/A[i*n+i]; 
    float relativeError = (X[i] - C[i])/X[i];
    ///printf("%f ", X[i]);
    if (relativeError > relativeTolerance
      || relativeError < -relativeTolerance) {
      printf("TEST FAILED\n\n");
      exit(0);
    }
  }
  printf("TEST PASSED\n\n");
  free(X);
}

void startTime(Timer* timer) {
    gettimeofday(&(timer->startTime), NULL);
}

void stopTime(Timer* timer) {
    gettimeofday(&(timer->endTime), NULL);
}

float elapsedTime(Timer timer) {
    return ((float) ((timer.endTime.tv_sec - timer.startTime.tv_sec) \
                + (timer.endTime.tv_usec - timer.startTime.tv_usec)/1.0e6));
}

