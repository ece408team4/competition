/******************************************************************************
 *cr
 *cr         (C) Copyright 2010-2013 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ******************************************************************************/


#include <stdio.h>
#include "support.h"
#include "kernel.cu"

#define BLOCK_SIZE 1024
//#define CHUNK_SIZE 1024

int main(int argc, char**argv) {

    Timer timer;
    cudaError_t cuda_ret;

    // Initialize host variables ----------------------------------------------

    printf("\nSetting up the problem..."); fflush(stdout);
    startTime(&timer);

    unsigned int n;
    if(argc == 1) {
        n = 10000;
    } else if(argc == 2) {
        n = atoi(argv[1]);
    } else {
        printf("\n    Invalid input parameters!"
           "\n    Usage: ./ldsolver              # Vector of size 10,000 is used"
           "\n    Usage: ./ldsolver <m>          # Vector of size m is used"
           "\n");
        exit(0);
    }

    float* A_h = (float*) malloc( sizeof(float)*n*n );
    for (unsigned int i=0; i < n; i++) {
        float diag = (rand() % 999) + 1;
	for(unsigned int j=0; j <= i; j++){
                 if(i == j) A_h[i*n+j] = diag;
		 else A_h[i*n+j] = ((rand()%50)/100)*diag/n;
	 }
    }
    float* B_h = (float*) malloc( sizeof(float)*n );
    for (unsigned int i=0; i < n; i++) { B_h[i] = (rand()%100)/100.00; }

    float* C_h = (float*) malloc( sizeof(float)*n );
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));
    printf("    Vector size = %u\n", n);

    // Allocate device variables ----------------------------------------------

    printf("Allocating device variables..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    float* A_d;
    cuda_ret = cudaMalloc((void**) &A_d, sizeof(float)*n*n );

    float* B_d;
    cudaMalloc((void**) &B_d, sizeof(float)*n);

    float* C_d;
    cudaMalloc((void**) &C_d, sizeof(float)*n);

    if(cuda_ret != cudaSuccess) FATAL("FAIL");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy host variables to device ------------------------------------------

    printf("Copying data from host to device..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cudaMemcpy(A_d, A_h, sizeof(float)*n*n, cudaMemcpyHostToDevice);
    cudaMemcpy(B_d, B_h, sizeof(float)*n, cudaMemcpyHostToDevice);

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Launch kernel ----------------------------------------------------------

    printf("Launching kernel..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE

    // Clear kernel runs a 1x1 block grid of BLOCK_SIZE threads
    dim3 blockDim(BLOCK_SIZE, 1, 1); 
    dim3 gridDim((int)ceil(n/(float)BLOCK_SIZE), 1, 1);

    clearC<<< gridDim, blockDim >>> (A_d, B_d, C_d, n); 

    // Transpose kernel divided matrix into CHUNK_SIZE by CHUNK_SIZE chunks
    /*
    blockDim.x = CHUNK_SIZE; blockDim.y = CHUNK_SIZE;
    gridDim.x = (int)ceil(n/(float)CHUNK_SIZE);
    gridDim.y = (int)ceil(n/(float)CHUNK_SIZE);

    transpose<<< gridDim, blockDim >>>(A_d, n);
    */

    int offset;

    /*
     * Iterate over matrix columns that are CHUNK_SIZE wide, launching a diagonal
     *  kernel and an appropriate number of blockAdd kernels for each column
     */
    for(offset=0; offset < n; offset += CHUNK_SIZE) {
      
      blockDim.x = CHUNK_SIZE; blockDim.y = CHUNK_SIZE;
      gridDim.x = 1; gridDim.y = 1;
      lowerDiag<<< gridDim, blockDim >>> (A_d, B_d, C_d, n, offset);

      cudaDeviceSynchronize();

      // on last column we don't need any blockAdd kernels, break out of loop
      if(offset+CHUNK_SIZE >= n)
        break;

      // enough blocks to cover to the bottom of the matrix
      gridDim.x = (int)ceil((n-offset-CHUNK_SIZE)/(float)CHUNK_SIZE); gridDim.y = 1;
      blockAdd<<< gridDim, blockDim >>> (A_d, B_d, C_d, n, offset);

      cudaDeviceSynchronize();

    }

    cuda_ret = cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy device variables to host ----------------------------------------

    printf("Copying data from device to host..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cudaMemcpy(C_h, C_d, sizeof(float)*n, cudaMemcpyDeviceToHost);

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Verify correctness -----------------------------------------------------

    printf("Verifying results..."); fflush(stdout);

    startTime(&timer);

    verify(A_h, B_h, C_h, n);

    stopTime(&timer); printf("... %f s\n", elapsedTime(timer));

    // Free memory ------------------------------------------------------------

    free(A_h);
    free(B_h);
    free(C_h);

    //INSERT CODE HERE
    cudaFree(A_d);
    cudaFree(B_d);
    cudaFree(C_d);

    return 0;

}

