/******************************************************************************
 *cr
 *cr         (C) Copyright 2010-2013 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ******************************************************************************/

// ============================================================================
// NOTE TO INSTRUCTORS: SOLUTION CODES ARE FOR INSTRUCTOR REFERENCE ONLY.
//    PLEASE DO NOT SHARE WITH STUDENTS
// ============================================================================

#define CHUNK_SIZE 32

/*
 * kernel to cover the diagonals of the matrix (CHUNK_SIZE x CHUNK_SIZE)
 *  performs the remaining partial product for each row, and finalizes elements
 *  of C row-by-row
 */
__global__ void lowerDiag(float* L, float* B, float* C, unsigned int n, int row){

    __shared__ float C_w[CHUNK_SIZE];

    int tx = threadIdx.x;
    int ty = threadIdx.y;

    // calculation for first row's solution element
    if(tx == 0 && ty == 0) 
      C[row] = (B[row] - C[row]) / L[row*n + row];

    __syncthreads();

    // load appropriate elements into shared memory
    if(ty == 0)
      C_w[tx] = C[row + tx];

    // loops over each column calculating partial product and finalize 1 element of C
    for(int j=0; j<blockDim.x; j++) {

        // if thread in correct column perform partial product
        if (tx == j && tx < ty) {
          atomicAdd(&(C_w[ty]), L[(ty + row)*n + tx + row] * C_w[j]);
        }

        __syncthreads();

        // have 1 thread  calculate next x element
        if(j+1<blockDim.x && row+j+1 < n && tx==ty && tx==j) {
          C_w[j+1] = (B[row+j+1] - C_w[j+1]) / L[(row+j+1)*n + (row+j+1)];
        }

        __syncthreads();
     }

    // write finalized values back to global memory
    if(tx == 0)
      C[row + ty] = C_w[ty];

}

/*
 * perform partial product of a block CHUNK_SIZE by CHUNK_SIZE, store result in C
 */
__global__ void blockAdd(float *L, float *B, float *C, unsigned int n, int row) {
    
    __shared__ float C_w[CHUNK_SIZE];
    __shared__ float C_r[CHUNK_SIZE];


    int r = (blockIdx.x + 1)*blockDim.y + threadIdx.y, tx = threadIdx.x;
    
    // threads out of matrix are not needed
    if(row + r > n)
      return;

    // load necessary values into shared memory and clear out temp shared memory
    if(threadIdx.y == 0) {
      C_w[tx] = 0;
      C_r[tx] = C[tx + row];
    }

   __syncthreads();

    // perform a partial product calculation and accumulate values into shared memory (row by row)
    atomicAdd(&(C_w[threadIdx.y]), L[(row + r)*n + tx + row] * C_r[tx]);

    __syncthreads();

    // write scratchpad values back to global memory
    if(threadIdx.y == 0) {
      atomicAdd(&(C[row + r + tx]), C_w[tx]);
    }

}

/*
 * Clear out solution vector C
 */
__global__ void clearC(float *L, float *B, float* C, unsigned int n) {

    int i = blockIdx.x*blockDim.x + threadIdx.x;

    if(i < n){
        C[i] = 0;
    }

}

/* 
 * perform transposition of L
 * NOT CALLED
 */
__global__ void transpose(float *L, unsigned int n) {

    int c = blockIdx.x*blockDim.x + threadIdx.x;
    int r = blockIdx.y*blockDim.y + threadIdx.y;

    // if this thread in lower diagonal swap its x,y coordinates and 0 out value
    if(c < n && r < n && threadIdx.x < threadIdx.y && blockIdx.x == blockIdx.y) {
      L[c*n + r] = L[r*n + c];
      L[r*n + c] = 0;
    } else if (c < n && r < n && blockIdx.x < blockIdx.y) {
      L[c*n + r] = L[r*n + c];
      L[r*n + c] = 0;
    }

}
